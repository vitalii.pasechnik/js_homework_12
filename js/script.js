"use strict";
/*1. Чому для роботи з input не рекомендується використовувати клавіатуру?

   При считывании текста с использованием событий клавиатуры иногда может не получиться адекватно обработать символы, которые вводит пользователь.
   Если нужно чтоб символы соответствовали выбранному языку на раскладке, то лучше делать проверку по event.key. 
   Если нужно чтоб символы не зависили от языка, то лучше делать проверку по event.code (для обработки спец символов), но на на разных конфигурациях клавиатур
   event.code может не соответствовать вводимому символу. По этому для обработки лучше использовать специально предназначенный для этого event - 'input',
   который адекватно и без проблем обрабатывает введенную пользователем информацию. 
*/

const buttons = document.body.querySelector('.btn-wrapper');

window.addEventListener('keydown', (e) => switchButton(e, buttons));

const switchButton = (e, buttons) => {
    const btnValues = [...buttons.children].map(button => button.textContent.toLowerCase());
    const selected = e.key.toLowerCase();
    btnValues.forEach((value, ind) => value !== selected ? buttons.children[ind].classList.remove('active') : buttons.children[ind].classList.add('active'));
}